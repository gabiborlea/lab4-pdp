﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Lab4
{
    public class TaskAsyncParser
    {
        public TaskAsyncParser(List<string> urls)
        {
            List<Task> tasks = new List<Task>();
            foreach (var url in urls)
            {
                tasks.Add(Task.Factory.StartNew(RunAsync, url));
            }
            Task.WaitAll(tasks.ToArray());
            Thread.Sleep(1000);
        }

        private async Task RunAsync(object urlObject)
        {
            string url = (string)urlObject;
            IPAddress IP = Dns.GetHostEntry(url.Split('/')[0]).AddressList[0];
            IPEndPoint endpoint = new IPEndPoint(IP, Utils.PORT);
            Socket socket = new Socket(IP.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            byte[] buffer = new byte[Utils.BUFFER_SIZE];

            await Connect(socket, endpoint);
            await Send(socket, url);
            await Receive(socket, buffer);
            socket.Shutdown(SocketShutdown.Both);

            Utils.viewResponse(buffer, url);

        }

        private async Task Connect(Socket socket, IPEndPoint endpoint)
        {
            TaskCompletionSource<int> promise = new TaskCompletionSource<int>();
            socket.BeginConnect(
                endpoint,
                (IAsyncResult asyncResult) => promise.SetResult(EndConnectWrapper(socket, asyncResult)),
                null
            );
            await promise.Task;
        }

        private int EndConnectWrapper(Socket socket, IAsyncResult asyncResult)
        {
            socket.EndConnect(asyncResult);
            return 1;
        }

        private async Task Send(Socket socket, string url)
        {
            TaskCompletionSource<int> promise = new TaskCompletionSource<int>();
            byte[] getRequest = Utils.get(url);
            socket.BeginSend(
                getRequest,
                0,
                getRequest.Length,
                0,
                (IAsyncResult asyncResult) => promise.SetResult(socket.EndSend(asyncResult)),
                null);
            await promise.Task;
        }

        private async Task Receive(Socket socket, byte[] buffer)
        {
            TaskCompletionSource<int> promise = new TaskCompletionSource<int>();
            socket.BeginReceive(
                buffer,
                0,
                Utils.BUFFER_SIZE,
                0,
                (IAsyncResult asyncResult) => promise.SetResult(socket.EndReceive(asyncResult)),
                null
            );
            await promise.Task;
        }
    }
}
