﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Lab4
{
    public class TaskParser
    {
        public TaskParser(List<string> urls)
        {
            List<Task> tasks = new List<Task>();
            foreach(var url in urls)
            {
                tasks.Add(Task.Factory.StartNew(Run, url));
            }
            Task.WaitAll(tasks.ToArray());
        }

        private void Run(object urlObject)
        {
            string url = (string)urlObject;
            IPAddress IP = Dns.GetHostEntry(url.Split('/')[0]).AddressList[0];
            IPEndPoint endpoint = new IPEndPoint(IP, Utils.PORT);
            Socket socket = new Socket(IP.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            byte[] buffer = new byte[Utils.BUFFER_SIZE];

            var res = Connect(socket, endpoint);
            TaskCompletionSource<bool> sendPromise = new TaskCompletionSource<bool>();
            TaskCompletionSource<bool> receivePromise = new TaskCompletionSource<bool>();
            res.ContinueWith((task)=> {
                if (task.Result)
                {
                    Send(socket, url).ContinueWith((task1) => sendPromise.SetResult(true));
                } else
                {
                    sendPromise.SetResult(false);
                }
            });
            sendPromise.Task.ContinueWith((task) => {
                if (task.Result)
                {
                    Receive(socket, buffer).ContinueWith((task1) => receivePromise.SetResult(true));
                } else
                {
                    receivePromise.SetResult(false);
                }
            }  
            );
            receivePromise.Task.ContinueWith((task) =>
            {
                if (task.Result)
                {
                    socket.Shutdown(SocketShutdown.Both);
                    Utils.viewResponse(buffer, url);
                }
            });

        }

        private Task<bool> Connect(Socket socket, IPEndPoint endpoint)
        {
            TaskCompletionSource<bool> promise = new TaskCompletionSource<bool>();
            socket.BeginConnect(
                endpoint,
                (IAsyncResult asyncResult) => {
                    promise.SetResult(EndConnectWrapper(socket, asyncResult));
                },
                null
            );
            return promise.Task;
        }

        private bool EndConnectWrapper(Socket socket, IAsyncResult asyncResult)
        {
            try
            {
                socket.EndConnect(asyncResult);
            } catch(Exception e)
            {
                return false;
            }
            
            return true;
        }

        private Task Send(Socket socket, string url)
        {
            TaskCompletionSource<bool> promise = new TaskCompletionSource<bool>();
            byte[] getRequest = Utils.get(url);
            socket.BeginSend(
                getRequest,
                0,
                getRequest.Length,
                0,
                (IAsyncResult asyncResult) => { socket.EndSend(asyncResult); promise.SetResult(true); },
                null);
            return promise.Task;
        }

        private Task Receive(Socket socket, byte[] buffer)
        {
            TaskCompletionSource<int> promise = new TaskCompletionSource<int>();
            socket.BeginReceive(
                buffer,
                0,
                Utils.BUFFER_SIZE,
                0,
                (IAsyncResult asyncResult) => promise.SetResult(socket.EndReceive(asyncResult)),
                null
            );
            return promise.Task;
        }
    }
}
