﻿using System;
using System.Collections.Generic;

namespace Lab4
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            List<string> urls = new List<string>()
            {
                 "www.cs.ubbcluj.ro/~forest/",
                "www.cs.ubbcluj.ro/~motogna/LFTC/",
                "www.cs.ubbcluj.ro/~rlupsa/edu/pdp/",
                "www.cs.ubbcluj.ro/~ilazar/ma/"
            };
            TaskParser taskParser = new TaskParser(urls);
            CallbackParser callbackParser = new CallbackParser(urls);
            TaskAsyncParser taskAsyncParser = new TaskAsyncParser(urls);
        }
    }
}
