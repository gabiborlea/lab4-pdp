﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Lab4
{
    public class CallbackParser
    {
        public CallbackParser(List<string> urls)
        {
            foreach (var url in urls)
            {
                Start(url);
                Thread.Sleep(500);
            }
        }

        private void Start(string url)
        {
            IPAddress IP = Dns.GetHostEntry(url.Split('/')[0]).AddressList[0];
            IPEndPoint endpoint = new IPEndPoint(IP, Utils.PORT);
            Socket socket = new Socket(IP.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.BeginConnect(endpoint, OnConnect, new Payload(socket, endpoint, url));
        }

        private void OnConnect(IAsyncResult payloadObject)
        {
            Payload payload = (Payload)payloadObject.AsyncState;
            byte[] getRequest = Utils.get(payload.url);
            payload.socket.BeginSend(getRequest, 0, getRequest.Length, 0, OnSend, payload);
        }

        private void OnSend(IAsyncResult payloadObject)
        {
            Payload payload = (Payload)payloadObject.AsyncState;
            int sentDataSize = payload.socket.EndSend(payloadObject);
            payload.socket.BeginReceive(payload.buffer, 0, Utils.BUFFER_SIZE, 0, OnReceive, payload);
        }

        private void OnReceive(IAsyncResult payloadObject)
        {
            Payload payload = (Payload)payloadObject.AsyncState;
            payload.socket.EndReceive(payloadObject);
            payload.socket.Shutdown(SocketShutdown.Both);
            payload.socket.Close();
            Utils.viewResponse(payload.buffer, payload.url);
        }
    }
}
