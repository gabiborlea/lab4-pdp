﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Lab4
{
    public class Utils
    {
        public static int PORT = 80;
        public static int BUFFER_SIZE = 1024;
        public static byte[] get(string url)
        {
            var pathDelimiterIndex = url.IndexOf('/');
            var endpoint = pathDelimiterIndex < 0 ? url : url.Substring(pathDelimiterIndex);
            var baseUrl = url.Split('/')[0];

            string getRequest = "GET " + endpoint + " HTTP/1.1\r\nHost: " + baseUrl + "\r\nContent-Length: 0\r\n\r\n";
            return Encoding.ASCII.GetBytes(getRequest);
        }
        public static void viewResponse(byte[] response, string url)
        {
            string content = Encoding.ASCII.GetString(response, 0, BUFFER_SIZE);
            string[] splitContent = content.Split('\r', '\n');
            string header = "";
            foreach (var line in splitContent)
            {
                if (line.Length == 0)
                {
                    continue;
                }

                if (line.StartsWith("Content-Length:"))
                {
                    header += "Length: " + line.Split(':')[1] + " bytes\n";
                }
                else
                {
                    header += line + "\n";
                    if (line.StartsWith("Content-Type:"))
                    {
                        break;
                    }
                }
            }
            Console.WriteLine(url + "\n" + header + "\n\n\n");
        }

    }

    public class Payload
    {
        public Socket socket;
        public IPEndPoint endpoint;
        public string url;
        public byte[] buffer = new byte[4096];

        public Payload(Socket socket, IPEndPoint endpoint, string url)
        {
            this.socket = socket;
            this.endpoint = endpoint;
            this.url = url;
        }
    }
}
